1. sudo systemctl status mysql
1. Run sudo mysql
1. show databases;
1. create database nc_coffee;
1. show databases;
1. use nc_coffee;
1. show tables;
1. create table coffee_table( id int, name varchar(255), region varchar(255), roast
varchar(255) );
1. show tables;
1. describe coffee_table;
    - the whole thing is called schema
1. insert into coffee_table values (1, "default route", "ethiopia", "light");
1. show tables;
1. describe coffee_table;
1. select * from coffee_table;
1. insert into coffee_table values (2, "docker run", "mexico", "medium");
1. insert into coffee_table values (3, "helpdesk", "honduras", "medium");
1. insert into coffee_table values (4, "on-call", "peru", "dark");
1. insert into coffee_table values (5, "ipconfig", "tanzania", "blonde");
1. insert into coffee_table values (6, "traceroute", "bali", "ned-dark");
1. show tables;
1. describe coffee_table;
1. select * from coffee_table;
1. select name from coffee_table;
1. create table customers( id int, firt_name varchar(255), last_name varchar(255), origin
varchar(255),age int, alias varchar(255) );
1. show tables;
1. describe customers;
1. insert into customers values (1, "thor", "odinson", "asgard", 1500, "strongest avenger");
1. insert into customers values (2, "clint", "barton", "earth", 35, "hawkeye");
1. insert into customers values (3, "tony", "stark", "earth", 52, "iron man");
1. insert into customers values (4, "peter", "parker", "earth", 17, "spiderman");
1. insert into customers values (5, "groot", "groot", "planet x", 18, "tree");
1. show tables;
1. describe customers;
1. select * from customers;
1. select firt_name from customers;
1. select * from customers where origin = "earth";
1. select * from customers where origin = "earth" or origin = "asgrad";
1. select * from customers where age < 30;
1. select * from customers where not origin = "earth" ;
1. insert into customers values (6, "jeff", "smith", "earth", 43, "jack the man");
1. select * from customers;
1. delete from customers where first_name = "jeff";
1. update customers set last_name = NULL where firt_name = "groot";
1. select * from customers order by age asc;
1. select * from customers order by age desc;
1. alter table customers add beard boolean;
1. select * from customers;
1. update customers set beard = True where firt_name = "thor";
1. update customers set beard = False where firt_name = "groot";
1. select * from customers;
update customers set beard = False where firt_name = "peter";
1. update customers set beard = False where firt_name = "tony"
1. select * from customers;